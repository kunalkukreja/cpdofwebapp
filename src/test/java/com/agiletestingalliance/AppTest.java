package com.qaagility.controller;

import org.junit.*;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
 
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import java.util.logging.Logger;

public class AppTest {

WebDriver driver;
	
	@Test	
      public void testCPDOF() {
	System.setProperty("webdriver.gecko.driver","/home/devops/Downloads/geckodriver");
	driver = new FirefoxDriver();
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://localhost:8080/WebApp/");

      
        WebElement learnMoreButton = driver.findElement(By.xpath("/html/body/div[2]/div/p[2]/a"));
        learnMoreButton.click();
	
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        String url = driver.getCurrentUrl();

	System.out.println("Current URL: " + url);
        assertEquals("Result", url, "http://cpdof.devopsppalliance.org/");

	driver.quit();
      }



}
