package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class MinimumTest {
	private Minimum minimum = new Minimum();

	@Test
	public void testGetMin1() {
		int result = minimum.getMax(1,2);
		assertEquals("Result", result, 2);
	}

	@Test
	public void testGetMin2() {
		int result = minimum.getMax(2,1);
		assertEquals("Result", result, 2);
	}

}
